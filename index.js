#!usr/bin/node

'use strict';

const Server = require('./server/');

global.tenebrific = new Server();

tenebrific.start();
