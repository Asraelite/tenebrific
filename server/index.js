const ServerInterface = require('./interface.js');

class Server extends ServerInterface {
	constructor() {
		super();
	}

	start() {
		this.log('Server started');
	}

	stop() {
		this.log('Server stopping');
	}
}

module.exports = Server;
